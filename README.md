# Onboard New Users with CSV

This script automates the process of onboarding users
to the Flywheel from a specified CSV file.
It validates the provided user information,
creates new users if they do not already exist in Flywheel,
and assigns them to specified groups and projects
with appropriate permissions.

## Prerequisites

- Python 3.6 or higher
- Access to a Flywheel instance
- Flywheel SDK installed (`pip install flywheel-sdk`)
- A valid Flywheel API key with site admin permissions

## CSV File Format

The script expects a CSV file with the following columns:

- Last Name
- First Name
- EMAIL
- Group ID
- Project Name
- Site-Role
- Project Permission

Ensure that the CSV file follows this
format and includes headers.
All fields are required for each user.

## Usage

Before running the script, ensure all prerequisites
are met and necessary packages are installed.

1. **Install Required Packages**:
If you have a `requirements.txt` file generated
from your `pyproject.toml` or otherwise provided,
install the required packages using pip.
Run the following command in your terminal:

    ```bash
    pip install -r requirements.txt
    ```

    This will install all the
dependencies listed in `requirements.txt`
into your current Python environment.

2. **Prepare Your CSV File** according to the format specified above.

3. **Obtain Your Flywheel API Key**:
This can typically be found in your
Flywheel account settings.

4. **Run the Script** with the following command,
replacing `<your-api-key>` with your actual Flywheel API key
and `<path-to-your-csv-file>` with the path to your CSV file:

```bash
python ./fw-onboard-users/onboard_users_to_project.py --key <your-api-key> --file <path-to-your-csv-file>
```

### Command-line Arguments

- `--key`: Your Flywheel API key. This is required.
- `--file`: The path to the CSV file
containing user information. This is required.

## Example

```bash
python ./fw-onboard-uers/onboard_users_to_project.py --key abcd1234 --file users_to_onboard.csv
```

## Output

The script will log its progress and any errors encountered.
If successful, users will be added or updated in Flywheel
according to the information in the provided CSV file.

If any errors are encountered during the process,
the script will produce an error log
CSV file named `add_user_permission_error_log_<timestamp>.csv`,
where `<timestamp>` is the date and time when the script was run.
This file will list all encountered errors,
including details such as the row index in the CSV
file, the error message,
and information about the user that caused the error.
This allows for easy identification and correction of
issues in the CSV file or with specific user data.
