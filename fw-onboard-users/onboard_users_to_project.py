#!/usr/bin/env python

import argparse
import logging
import os
import re
import pandas as pd
import datetime
from pathlib import Path
import flywheel

# Setup logging
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)
log = logging.getLogger(__name__)

# Constants
SITE_PERM_ORDER = ["user", "developer", "site_admin"]
PERMISSION_LABEL_MAPPING = {
    "admin": "admin",
    "read/write": "read-write",
    "read": "read-only",
}
CSV_COLUMN_LIST = [
    "Last Name",
    "First Name",
    "EMAIL",
    "Group ID",
    "Project Name",
    "Site-Role",
    "Project Permission",
]


def validate_file_type(input_file):
    """Validate the CSV file format and columns."""
    if not os.path.exists(input_file):
        raise argparse.ArgumentTypeError(f"{input_file} does not exist.")
    if not input_file.endswith(".csv"):
        raise argparse.ArgumentTypeError(f"{input_file} is not a CSV file.")
    df = pd.read_csv(input_file)
    missing_columns = [col for col in CSV_COLUMN_LIST if col not in df.columns]
    if missing_columns:
        raise argparse.ArgumentTypeError(
            f"CSV file is missing required columns: {', '.join(missing_columns)}"
        )
    return input_file


def validate_email(email_add):
    regex = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}"

    if not (re.fullmatch(regex, email_add)):
        log.error(
            f"{email_add} is not a valid email address. Please ensure it is in this following format - "
            f"<abc@somedomain.com>"
        )
        return False
    return True


def find_or_add_user(fw, index, firstname, lastname, email, site_role):
    """Find or create new user object with specified site role"""
    try:
        user_obj = fw.get_user(email)
    except flywheel.ApiException:
        log.info(f"{email} not found in the instance. Adding user...")
        siterole = (
            "user"
            if (site_role == "nan" or site_role not in SITE_PERM_ORDER)
            else site_role
        )
        if firstname and lastname and email and siterole:
            user_object = flywheel.User(
                id=email,
                email=email,
                firstname=firstname.title(),
                lastname=lastname.title(),
                roles=siterole,
            )
            try:
                new_user = fw.add_user(user_object)

            except Exception as e:
                log.error(
                    f"Error occurred. Unable to add user {firstname}, {lastname}. Details: {str(e)}"
                )
                return None
            else:
                return new_user
        else:
            log.error(f"Missing information on the file. Unable to add user {index}")
            return None
    except Exception as e:
        log.error(f"Error occurred while getting the user. Details: {str(e)}")
        return None
    else:
        log.info(f"Added user {email}.")
        return user_obj


def is_group_valid(fw, group_id):
    """Check if group id is valid"""
    try:
        log.info(f"Validating id {group_id} exists in instance...")
        fw_group = fw.get_group(group_id)
        return fw_group
    except Exception as e:
        log.error(f"Invalid group-{group_id}. Error: {str(e)}")
        return None


def find_project_roles_id(client, proj_permission):
    tmp_proj_permission = PERMISSION_LABEL_MAPPING[proj_permission]
    role_list = list()
    log.info(f"\tFinding role id for {proj_permission} in Instance")
    for i in client.get_all_roles():
        if i["label"] == tmp_proj_permission:
            role_list.append(i["_id"])

    if role_list:
        return role_list
    else:
        return None


def add_user_to_proj(fw, user_email, grp_id, proj_name, proj_permission):
    """Add user to project container"""
    group_container = is_group_valid(fw, grp_id)

    if group_container:
        try:
            log.info(f"Finding {proj_name} in {grp_id} group container")
            project = group_container.projects.find_one(f"label={proj_name}")
        except ValueError:
            log.warning(
                f"Project {proj_name} not found in the group container {grp_id}"
            )
            log.info(f"Creating project {proj_name} in the group container {grp_id}...")
            project = group_container.add_project(label=proj_name)
        except Exception:
            raise Exception(f"Uncaught Exception when finding project {proj_name}")
        # Get the role id within the instance
        role_list = find_project_roles_id(fw, proj_permission)

        if role_list:
            try:
                log.info(f"\tAdding {user_email} to project {project.label}...")

                fw.add_project_permission(
                    project.id, {"_id": user_email, "role_ids": role_list}
                )
            except flywheel.ApiException:
                raise
            except Exception as e:
                raise Exception(
                    f"\t\tError in adding {user_email} to {project.label} project.Details: {str(e)}"
                )
            else:
                log.info(
                    f"\t\tSuccessfully added user to the {project.label} project container as {proj_permission}"
                )

        else:
            raise ValueError(
                f"*{proj_permission} is not found in the roles. Unable to add the user to the roles*"
            )

    else:
        raise ValueError(
            f"Unable to retrieve the group container:{grp_id} from the instance"
        )


def main(api_key, csv_path):
    fw = flywheel.Client(api_key, root=True)

    user_roles = fw.get_current_user().roles

    if "site_admin" in user_roles:
        error_log_df = pd.DataFrame()
        df = pd.read_csv(Path(csv_path)).dropna(how="all")

        for (
            index,
            lastname,
            firstname,
            email,
            grp_id,
            proj_name,
            site_role,
            proj_permission,
        ) in df.itertuples():
            error_log = dict()
            log.info(f"Loading Row {index + 1}...")
            # Make sure there is value in all column
            if (
                not pd.isna(firstname)
                and not pd.isna(lastname)
                and not pd.isna(email)
                and not pd.isna(site_role)
                and not pd.isna(grp_id)
                and not pd.isna(proj_name)
                and not pd.isna(proj_permission)
            ):
                email = email.strip()
                if validate_email(email):
                    log.info(f"Finding or adding user with {email} to the instance")

                    if site_role.strip().lower() == "site admin":
                        site_role.replace(" ", "_")

                    if proj_permission.strip().lower() not in list(
                        PERMISSION_LABEL_MAPPING.keys()
                    ):
                        error_log["row_index"] = int(index)
                        permission_str = ", ".join(
                            list(PERMISSION_LABEL_MAPPING.keys())
                        )
                        error_msg_str = f"Invalid project permission being provided. Only {permission_str} are allowed...Skipping..."
                        error_log["error_msg"] = (
                            f"Invalid project permission was provided. Only {permission_str} are allowed"
                        )
                        error_log["username"] = firstname + " " + lastname
                        log.error(error_msg_str)
                        error_log_df = error_log_df.append(error_log, ignore_index=True)
                        continue

                    user_obj = find_or_add_user(
                        fw,
                        index,
                        firstname.strip().title(),
                        lastname.strip().title(),
                        email,
                        site_role.strip().lower(),
                    )
                    if user_obj:
                        try:
                            # add user to the project container
                            add_user_to_proj(
                                fw,
                                email,
                                grp_id.strip(),
                                proj_name.strip(),
                                proj_permission.strip().lower(),
                            )
                        except Exception as e:
                            error_log["row_index"] = int(index)
                            error_log["error_msg"] = str(e).strip()
                            error_log["username"] = firstname + " " + lastname
                            log.error(f"{e}. Skipping....")
                            error_log_df = error_log_df._append(
                                error_log, ignore_index=True
                            )
                            continue
                else:
                    error_log["row_index"] = int(index)
                    error_log["error_msg"] = (
                        f"Invalid email address was provided {email}"
                    )
                    error_log["username"] = firstname + " " + lastname
                    log.error(f"Skipping {index + 1} row....")
                    error_log_df = error_log_df.append(error_log, ignore_index=True)
            else:
                error_log["row_index"] = int(index)
                error_log["error_msg"] = "Found empty value"
                error_log["username"] = firstname + " " + lastname
                log.error(f"Found empty value on {index + 1} row. Skipping...")
                error_log_df = error_log_df.append(error_log, ignore_index=True)
        if not error_log_df.empty:
            time_now = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
            error_log_df.to_csv(
                f"add_user_permission_error_log_{time_now}.csv", index=False
            )
    else:
        log.error(
            "You do not have the permission to add user to the instance. Please contact the site admin."
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Script to onboard users to Flywheel platform from a CSV file."
    )
    parser.add_argument("--key", required=True, help="API Key for Flywheel.")
    parser.add_argument(
        "--file",
        dest="input",
        required=True,
        type=validate_file_type,
        help="CSV file for adding user to instance.",
        metavar="CSV File",
    )

    args = parser.parse_args()
    main(args.key, args.input)
